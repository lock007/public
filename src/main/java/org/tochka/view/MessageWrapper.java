package org.tochka.view;

import java.util.Date;

/**
 * @author Алексей
 * @since 18.10.2017.
 */
public interface MessageWrapper {

    String getTitle();

    void setTitle(String title);

    String getDescription();

    void setDescription(String description);

    Date getPubDate();

    void setPubDate(Date date);

    void setFeed(FeedWrapper feed);

}
