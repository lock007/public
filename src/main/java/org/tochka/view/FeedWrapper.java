package org.tochka.view;

import org.tochka.db.model.Author;

/**
 * @author Алексей
 * @since 18.10.2017.
 */
public interface FeedWrapper {

    Long getId();

    String getUri();

    void setUri(String uri);

    String getTitle();

    String getDescription();

    void setAuthor(String author);

    String getAuthor();

    void setTitle(String title);

    void setDescription(String description);

    void clearAuthors();

    void addAuthor(Author author);

    String getParsingTemplate();

    void setParsingTemplate(String parsingTemplate);

}
