package org.tochka.view.impl;

import org.tochka.db.model.Message;
import org.tochka.view.FeedWrapper;
import org.tochka.view.MessageWrapper;

import java.util.Date;

/**
 * @author Алексей
 * @since 18.10.2017.
 */
public class MessageWrapperImpl implements MessageWrapper {

    private Message entity;

    public MessageWrapperImpl(Message entity) {
        this.entity = entity;
    }

    public Message getEntity() {
        return entity;
    }

    public String getTitle() {
        return entity.getTitle();
    }

    public void setTitle(String title) {
        entity.setTitle(title);
    }

    public String getDescription() {
        return entity.getDescription();
    }

    public void setDescription(String description) {
        entity.setDescription(description);
    }

    @Override
    public Date getPubDate() {
        return entity.getPubDate();
    }

    @Override
    public void setPubDate(Date date) {
        entity.setPubDate(date);
    }

    @Override
    public void setFeed(FeedWrapper feed) {
        entity.setFeed(((FeedWrapperImpl) feed).getEntity());
    }

}
