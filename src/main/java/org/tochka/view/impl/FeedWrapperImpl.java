package org.tochka.view.impl;

import org.tochka.db.model.Author;
import org.tochka.db.model.Feed;
import org.tochka.view.FeedWrapper;

/**
 * @author Алексей
 * @since 18.10.2017.
 */
public class FeedWrapperImpl implements FeedWrapper {

    private Feed entity;

    public FeedWrapperImpl(Feed entity) {
        this.entity = entity;
    }

    public Feed getEntity() {
        return entity;
    }

    @Override
    public String getUri() {
        return entity.getUri();
    }

    @Override
    public void setUri(String uri) {
        entity.setUri(uri);
    }

    @Override
    public String getTitle() {
        return entity.getTitle();
    }

    @Override
    public String getDescription() {
        return entity.getDescription();
    }

    @Override
    public Long getId() {
        return entity.getId();
    }

    @Override
    public void setAuthor(String author) {
        entity.setAuthor(author);
    }

    @Override
    public String getAuthor() {
        return entity.getAuthor();
    }

    @Override
    public void setTitle(String title) {
        entity.setTitle(title);
    }

    @Override
    public void setDescription(String description) {
        entity.setDescription(description);
    }

    @Override
    public void clearAuthors() {
        entity.getAuthors().clear();
    }

    @Override
    public void addAuthor(Author author) {
        entity.getAuthors().add(author);
    }

    @Override
    public String getParsingTemplate() {
        return entity.getParsingTemplate();
    }

    @Override
    public void setParsingTemplate(String parsingTemplate) {
        entity.setParsingTemplate(parsingTemplate);
    }

}
