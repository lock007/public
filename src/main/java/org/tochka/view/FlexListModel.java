package org.tochka.view;

import org.zkoss.zul.AbstractListModel;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Supplier;

/**
 * @author Алексей
 * @since 21.10.2017.
 */
public class FlexListModel<T> extends AbstractListModel<T> {

    private final static int PAGE_SIZE = 100;
    private final static int CACHE_SIZE = 2;
    private Map<Integer, List<T>> _elements = new LinkedHashMap<>();
    private Supplier<Integer> sizeSp;
    private BiFunction<Integer, Integer, List<T>> fnLoader;

    public FlexListModel(BiFunction<Integer, Integer, List<T>> fnLoader, Supplier<Integer> sizeSp) {
        this.sizeSp = sizeSp;
        this.fnLoader = fnLoader;
    }

    @Override
    public T getElementAt(int index) {
        int page = index - index % PAGE_SIZE;
        List<T> ts = _elements.get(page);
        if(ts == null)
            ts = fnLoader.apply(page, page + PAGE_SIZE);
        _elements.put(page, ts);
        if(_elements.size() > CACHE_SIZE) {
            Iterator<?> iterator = _elements.entrySet().iterator();
            iterator.next();
            iterator.remove();
        }
        return ts.get(index - page);
    }

    @Override
    public int getSize() {
        return sizeSp.get();
    }

}