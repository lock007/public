package org.tochka.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Алексей
 * @since 21.10.2017.
 */
@Entity
@Table(name = "message_content")
public class Content implements BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "owner_id", nullable = false)
    private Message owner;

    @Column
    private String type;

    @Column(columnDefinition = "TEXT")
    private String value;

    public Content() {
    }

    public Content(Message owner) {
        this.owner = owner;
    }

    @Override
    public Long getId() {
        return id;
    }

    public Message getOwner() {
        return owner;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
