package org.tochka.db.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * @author Алексей
 * @since 18.10.2017.
 */
@Entity
@Table(name = "message")
public class Message implements BaseEntity {

    public Message() {
    }

    public Message(Feed feed) {
        this.feed = feed;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String title;

    @Column(length = 1000)
    private String uri;

    @Column(length = 1000)
    private String author;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Column(name = "pub_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pubDate;

    @Column(columnDefinition = "TEXT")
    private String content;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
    private Collection<Content> contents;

    /**
     * Связь с feed только здесь, дабы избежать загрузки потенциально большой коллекции сообщений у feed
     */
    @ManyToOne
    @JoinColumn(name = "feed_id", nullable = false)
    private Feed feed;

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public Feed getFeed() {
        return feed;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Collection<Content> getContents() {
        if (contents == null) {
            contents = new ArrayList<>();
        }

        return contents;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

}
