package org.tochka.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Алексей
 * @since 21.10.2017.
 */
@Entity
@Table(name = "feed_author")
public class Author implements BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "owner_id", nullable = false)
    private Feed owner;

    @Column(length = 1000)
    private String name;

    @Column(length = 1000)
    private String email;

    @Column(length = 1000)
    private String uri;

    public Author() {
    }

    public Author(Feed owner) {
        this.owner = owner;
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Feed getOwner() {
        return owner;
    }

}
