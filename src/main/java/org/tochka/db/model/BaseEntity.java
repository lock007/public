package org.tochka.db.model;

/**
 * @author Алексей
 * @since 18.10.2017.
 */
public interface BaseEntity {

    Long getId();

}
