package org.tochka.db.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * @author Алексей
 * @since 18.10.2017.
 */
@Entity
@Table(name = "feed")
public class Feed implements BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 1000)
    private String title;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Column(length = 1000)
    private String uri;

    @Column(length = 1000)
    private String author;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Collection<Author> authors;

    @Temporal(TemporalType.TIME)
    @Column(name = "pub_date")
    private Date pubDate;

    @Column(columnDefinition = "TEXT")
    private String parsingTemplate;

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Collection<Author> getAuthors() {
        if (authors == null) {
            authors = new ArrayList<>();
        }

        return authors;
     }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public String getParsingTemplate() {
        return parsingTemplate;
    }

    public void setParsingTemplate(String parsingTemplate) {
        this.parsingTemplate = parsingTemplate;
    }
}