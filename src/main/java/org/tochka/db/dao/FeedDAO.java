package org.tochka.db.dao;

import org.tochka.db.model.BaseEntity;
import org.tochka.db.model.Feed;
import org.tochka.db.model.Message;

import java.util.Collection;

/**
 * @author Алексей
 * @since 18.10.2017.
 */
public interface FeedDAO {

    <T extends BaseEntity> T merge(T entity);

    Collection<Feed> allFeeds();

    Collection<Message> findMessages(Feed feed, String pattern, int offset, int limit);

    int findMessagesResultSize(Feed feed, String pattern);

    boolean isMessageExists(Long feedId, String uri);

}
