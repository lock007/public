package org.tochka.db.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.tochka.db.dao.FeedDAO;
import org.tochka.db.model.BaseEntity;
import org.tochka.db.model.Feed;
import org.tochka.db.model.Message;

import java.util.Collection;

/**
 * @author Алексей
 * @since 18.10.2017.
 */
@Repository
public class FeedDAOImpl implements FeedDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private Session withSession() {
        return sessionFactory.getCurrentSession();
    }

    public <T extends BaseEntity> T merge(T entity) {
        return (T) withSession().merge(entity);
    }

    @Override
    public Collection<Feed> allFeeds() {
        return withSession().createQuery("from Feed f order by f.title").list();
    }

    @Override
    public Collection<Message> findMessages(Feed feed, String pattern, int offset, int limit) {
        return withSession()
                .createQuery("select m from Message m join m.feed f " +
                        "where f.id = ? and lower(m.description) like ? " +
                        "order by m.pubDate desc")
                .setParameter(0, feed.getId())
                .setParameter(1, "%" + pattern.toLowerCase() + "%")
                .setFirstResult(offset)
                .setMaxResults(limit)
                .list();
    }

    @Override
    public int findMessagesResultSize(Feed feed, String pattern) {
        return ((Number) withSession()
                .createQuery("select count(m.id) " +
                        "from Message m join m.feed f " +
                        "where f.id = ? and lower(m.description) like ?")
                .setParameter(0, feed.getId())
                .setParameter(1, "%" + pattern.toLowerCase() + "%")
                .uniqueResult()).intValue();
    }

    @Override
    public boolean isMessageExists(Long feedId, String uri) {
        return withSession().createQuery("select m.id from Message m join m.feed f where f.id = ? and m.uri = ?")
                .setParameter(0, feedId)
                .setParameter(1, uri).list().size() > 0;
    }

}