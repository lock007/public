package org.tochka.service.impl;

import org.tochka.db.model.Message;
import org.tochka.service.FeedController;
import org.tochka.service.parser.ChannelTO;
import org.tochka.service.parser.ItemTO;
import org.tochka.service.parser.ParserUtils;
import org.tochka.service.parser.RssTO;
import org.tochka.view.FeedWrapper;
import org.tochka.view.impl.MessageWrapperImpl;

/**
 * @author Алексей
 * @since 21.10.2017.
 */
public class FeedParser {

    private FeedWrapper feed;

    public FeedParser(FeedWrapper feed) {
        this.feed = feed;
    }

    public void parseAndSaveFeed(FeedController controller) throws Exception {
        RssTO rss = ParserUtils.read(feed.getUri(), feed.getParsingTemplate());
        if (rss.getChannel() != null) {
            ChannelTO channel = rss.getChannel();
            feed.setTitle(channel.getTitle());
            feed.setDescription(channel.getDescription());
            controller.saveFeed(feed);

            if (channel.getItems() != null) {
                for (ItemTO item : channel.getItems()) {
                    Message message = ParserUtils.mapMessage(item);
                    if (!controller.isMessageExists(feed.getId(), message.getUri())) {
                        MessageWrapperImpl msgWrap = new MessageWrapperImpl(message);
                        msgWrap.setFeed(feed);
                        controller.saveMessage(msgWrap);
                    }
                }
            }
        }
    }

}
