package org.tochka.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.tochka.db.dao.FeedDAO;
import org.tochka.db.model.Feed;
import org.tochka.service.FeedController;
import org.tochka.view.FeedWrapper;
import org.tochka.view.MessageWrapper;
import org.tochka.view.impl.FeedWrapperImpl;
import org.tochka.view.impl.MessageWrapperImpl;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Алексей
 * @since 18.10.2017.
 */
@Service("feedController")
@Transactional(readOnly = true)
public class FeedControllerImpl implements FeedController {

    @Autowired
    private FeedDAO dao;

    @Override
    @Transactional
    public FeedWrapper saveFeed(FeedWrapper feed) {
        return new FeedWrapperImpl(dao.merge(((FeedWrapperImpl)feed).getEntity()));
    }

    @Override
    @Transactional
    public MessageWrapper saveMessage(MessageWrapper message) {
        return new MessageWrapperImpl(dao.merge(((MessageWrapperImpl)message).getEntity()));
    }

    @Override
    public List<MessageWrapper> findMessages(FeedWrapper feed, String pattern, int offset, int limit) {
        return dao.findMessages(((FeedWrapperImpl)feed).getEntity(), pattern, offset, limit).stream()
                .map(MessageWrapperImpl::new)
                .collect(Collectors.toList());
    }

    @Override
    public int totalSize(FeedWrapper feed, String searchPattern) {
        return dao.findMessagesResultSize(((FeedWrapperImpl)feed).getEntity(), searchPattern);
    }

    @Override
    public Collection<FeedWrapper> allFeeds() {
        return dao.allFeeds().stream()
                .map(FeedWrapperImpl::new)
                .collect(Collectors.toList());
    }

    @Override
    public FeedWrapper newFeed() {
        return new FeedWrapperImpl(new Feed());
    }

    @Override
    @Transactional
    public void parseRss(FeedWrapper feed) throws Exception {
        FeedParser parser = new FeedParser(feed);
        parser.parseAndSaveFeed(this);
    }

    @Override
    public boolean isMessageExists(Long feedId, String uri) {
        return dao.isMessageExists(feedId, uri);
    }

}