package org.tochka.service.parser;

import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.XMLContext;
import org.tochka.db.model.Feed;
import org.tochka.db.model.Message;
import org.xml.sax.InputSource;

import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.function.Supplier;

public class ParserUtils {

    private static SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.ENGLISH);

    /**
     * Шаблон по-умолчанию
     *
     * @return
     * @throws Exception
     */
    public static Unmarshaller getUnmarshaller() throws Exception {
        return getUnmarshaller(() -> {
            Mapping mapping = new Mapping();
            try {
                mapping.loadMapping(ParserUtils.class.getClassLoader()
                        .getResource("rss-mapping.xml").toExternalForm());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mapping;
        });
    }

    /**
     * Шаблон из переданной строки
     *
     * @param xmlString
     * @return
     * @throws Exception
     */
    public static Unmarshaller getUnmarshaller(String xmlString) throws Exception {
        if (xmlString != null && !xmlString.isEmpty()) {
            InputSource inputSource = new InputSource(new StringReader(xmlString));
            return getUnmarshaller(() -> {
                Mapping mapping = new Mapping();
                try {
                    mapping.loadMapping(inputSource);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return mapping;
            });
        } else {
            return getUnmarshaller();
        }
    }

    private static Unmarshaller getUnmarshaller(Supplier<Mapping> mapping) throws Exception {
        Unmarshaller unmarshaller;

        XMLContext context = new XMLContext();
        context.addMapping(mapping.get());

        unmarshaller = context.createUnmarshaller();
        unmarshaller.setClass(RssTO.class);

        return unmarshaller;
    }

    public static Date parseDate(String raw) {
        if (raw != null) {
            try {
                return sdf.parse(raw);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public static RssTO read(String uri, String parsingTemplate) throws Exception {
        URL url = new URL(uri);
        try (Reader reader = new InputStreamReader(url.openStream())) {
            return (RssTO) getUnmarshaller(parsingTemplate).unmarshal(reader);
        }
    }

    public static Feed mapFeed(ChannelTO channel) {
        Feed feed = new Feed();
        feed.setTitle(channel.getTitle());
        feed.setUri(channel.getLink());
        feed.setDescription(channel.getDescription());
        feed.setPubDate(parseDate(channel.getPubDate()));
        return feed;
    }

    public static Message mapMessage(ItemTO item) {
        Message message = new Message();
        message.setTitle(item.getTitle());
        message.setDescription(item.getDescription());
        message.setPubDate(parseDate(item.getPubDate()));
        message.setUri(item.getLink());

        return message;
    }

}