package org.tochka.service.parser;

import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.XMLContext;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.temporal.ChronoField;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ParserUtilsTest {

    private Unmarshaller unmarshaller;

    @Before
    public void init() throws Exception {
        unmarshaller = ParserUtils.getUnmarshaller();
    }

    private Reader readFile(String fileName) {
        return new BufferedReader(new InputStreamReader(getClass()
                .getClassLoader().getResourceAsStream(fileName)));
    }

    @Test
    public void transferSimpleTest() throws Exception {
        RssTO rss = (RssTO) unmarshaller
                .unmarshal(readFile("mocks/rss-sample.xml"));
        assertEquals("2.0", rss.getVersion());
    }

    @Test
    public void transferComplexTest() throws Exception {
        RssTO rss = (RssTO) unmarshaller
                .unmarshal(readFile("mocks/rss-other-sample.xml"));
        assertEquals("2.0", rss.getVersion());
        assertEquals(rss.getChannel().getTitle(), "Sample Feed - Favorite RSS");
        assertEquals(rss.getChannel().getItems().size(), 3);
        assertEquals(rss.getChannel().getItems().iterator().next().getTitle(), "RSS Resources");
    }

    @Test
    public void dateParseTest() {
        Date date = ParserUtils.parseDate("Tue, 26 Oct 2004 14:06:44 -0500");
        assertNotNull(date);

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        assertEquals(c.get(Calendar.YEAR), 2004);
    }

    public static Unmarshaller getUnmarshallerWrongLink() throws Exception {
        Unmarshaller unmarshaller;

        XMLContext context = new XMLContext();
        Mapping mapping = new Mapping();
        mapping.loadMapping(ParserUtils.class.getClassLoader()
                .getResource("mocks/rss-mapping-wrong-link.xml").toExternalForm());
        context.addMapping(mapping);

        unmarshaller = context.createUnmarshaller();
        unmarshaller.setClass(RssTO.class);

        return unmarshaller;
    }

    @Test
    public void transferWrongLinkTest() throws Exception {
        Unmarshaller unmarshaller = getUnmarshallerWrongLink();
        RssTO rss = (RssTO) unmarshaller
                .unmarshal(readFile("mocks/rss-sample.xml"));
        assertEquals("https://www.w3schools.com", rss.getChannel().getDescription());
    }

}