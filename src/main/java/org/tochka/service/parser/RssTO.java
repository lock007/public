package org.tochka.service.parser;

public class RssTO {

    private String version;

    private ChannelTO channel;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public ChannelTO getChannel() {
        return channel;
    }

    public void setChannel(ChannelTO channel) {
        this.channel = channel;
    }

}
