package org.tochka.service;

import org.tochka.view.FeedWrapper;
import org.tochka.view.MessageWrapper;

import java.util.Collection;
import java.util.List;

/**
 * @author Алексей
 * @since 18.10.2017.
 */
public interface FeedController {

    FeedWrapper saveFeed(FeedWrapper feed);

    MessageWrapper saveMessage(MessageWrapper message);

    Collection<FeedWrapper> allFeeds();

    FeedWrapper newFeed();

    void parseRss(FeedWrapper feed) throws Exception;

    boolean isMessageExists(Long feedId, String uri);

    List<MessageWrapper> findMessages(FeedWrapper feed, String pattern, int offset, int limit);

    int totalSize(FeedWrapper selectedFeed, String searchPattern);

}
