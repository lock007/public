package org.tochka;

import org.tochka.service.FeedController;
import org.tochka.view.FeedWrapper;
import org.tochka.view.FlexListModel;
import org.tochka.view.MessageWrapper;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Messagebox;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;

public class MainVM {

    @WireVariable("feedController")
    private FeedController controller;

    private FeedWrapper selectedFeed;

    private MessageWrapper selectedMessage;

    private String searchPattern = "";

    @Command
    void pushRss(@BindingParam("rssUri") String rssUri) {
    }

    public Collection<FeedWrapper> getFeeds() {
        return controller.allFeeds();
    }

    public FeedWrapper getSelectedFeed() {
        return selectedFeed;
    }

    public void setSelectedFeed(FeedWrapper selectedFeed) {
        this.selectedFeed = selectedFeed;
    }

    @Command
    @NotifyChange("selectedFeed")
    public void newFeed() {
        selectedFeed = controller.newFeed();
    }

    @Command
    @NotifyChange({"selectedFeed", "feeds"})
    public void saveFeed() {
        controller.saveFeed(selectedFeed);
    }

    @Command
    @NotifyChange({"selectedFeed", "feeds"})
    public void fetchSelected() {
        try {
            controller.parseRss(selectedFeed);
        } catch (Exception e) {
            e.printStackTrace();
            Messagebox.show(String.format("Error while fetching feed: %s", e.getMessage()));
        }
    }

    public MessageWrapper getSelectedMessage() {
        return selectedMessage;
    }

    public void setSelectedMessage(MessageWrapper selectedMessage) {
        this.selectedMessage = selectedMessage;
    }

    @DependsOn({"selectedFeed", "searchPattern"})
    public FlexListModel<MessageWrapper> getMessages() {
        if (selectedFeed != null) {
            int size = controller.totalSize(selectedFeed, searchPattern);
            return new FlexListModel<>(
                    (from, to) ->
                            controller.findMessages(selectedFeed, searchPattern, from, to - from),
                    () -> size
            );
        }
        return null;
    }

    public String getSearchPattern() {
        return searchPattern;
    }

    public void setSearchPattern(String searchPattern) {
        this.searchPattern = searchPattern;
    }

    @Command
    @NotifyChange("selectedFeed")
    public void doSearch() {}

    @Command
    @NotifyChange("selectedFeed")
    public void uploadRules(@BindingParam("event") UploadEvent event) {
        if (event.getMedia() == null || !event.getMedia().isBinary()) {
            selectedFeed.setParsingTemplate(event.getMedia().getStringData());
        }
    }

    @Command
    public void downloadDefaultRules() {
        Reader reader = new BufferedReader(new InputStreamReader(getClass()
                .getClassLoader().getResourceAsStream("rss-mapping.xml")));
        Filedownload.save(reader, "application/xml", "parsing-rules.xml");
    }

}